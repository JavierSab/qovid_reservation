# (Q)ovid Reservation
Django based project for demonstrating skills and style.

### Specification

This project must fulfill at least these functions:
- A patient must be able to request for a date for a Covid Vaccine. This must retrieve the patient a code that should
afterwards be validated.
- A patient must, afterwards, be able to validate his date when entering the vaccination place.

### Project Structure

For the stated purposes, we are developing a django based server structured as:
- A *reservations* app that will hold the views and models for applying for a date in the vaccination centre
as well as holding the relevant historical data, and for validating an access code for accessing the
vaccination centre.

### Milestones
- Build base django project and initial *readme.md* file.
- Implement the base django backend for the 2 main stated functionalities. Placehold integration with external
services.
- Implement a testing suite.
- Implement postgres integration and add *.gitlab-ci.yml* file to allow this project to run docker-based (using
tiangolo's [uwsgi-nginx](https://hub.docker.com/r/tiangolo/uwsgi-nginx/) as base.
- Add and installation guide for deploying the project in a fresh ubuntu machine.
- Implement a login system. Assuming that validation of the reservations in the vaccination sites will be done in fixed
machines, implement special users for performing those validations and/or allow for special IP permissions.
Probably a JWT based login system could work best depending on the chosen frontend, but in this scope we will use the
default provided in *django rest auth*.
- Start a frontend simple site for interacting with the backend server.