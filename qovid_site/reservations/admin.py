from django.contrib import admin

# Register your models here.

from .models import *

from django.utils.html import format_html
from django.urls import reverse

def linkify(field_name):
    """
    Converts a foreign key value into clickable links.

    If field_name is 'parent', link text will be str(obj.parent)
    Link will be admin url for the admin url for obj.parent.id:change
    """

    def _linkify(obj):
        linked_obj = getattr(obj, field_name)
        if linked_obj is None:
            return '-'
        app_label = linked_obj._meta.app_label
        model_name = linked_obj._meta.model_name
        view_name = f'admin:{app_label}_{model_name}_change'
        link_url = reverse(view_name, args=[linked_obj.pk])
        return format_html('<a href="{}">{}</a>', link_url, linked_obj)

    _linkify.short_description = field_name  # Sets column name
    return _linkify


class PatientAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'surname1', 'surname2', 'nhc_code', 'email')
    search_fields = ('name', 'surname1', 'surname2', 'nhc_code', 'email')


class VaccinationLocationAdmin(admin.ModelAdmin):

    list_display = ('pk', 'name', 'starting_time', 'ending_time', 'capacity')
    search_fields = ('name',)


class ReservationAdmin(admin.ModelAdmin):
    list_display = ('pk', linkify('patient'), linkify('place'), 'validation_code',
                    'date', 'confirmed', 'attended', 'created', 'modified',)


admin.site.register(Patient, PatientAdmin)
admin.site.register(VaccinationLocation, VaccinationLocationAdmin)
admin.site.register(Reservation, ReservationAdmin)