from django.db import models

# Create your models here.


class Model(models.Model):
    """ Redeclaring Model class is a workarround to allow Pycharm to properly interpretate the 'objects' attribute for
    any model."""
    objects = models.Manager()

    class Meta:
        abstract = True


class VaccinationLocation(Model):
    """This table should hold all the available places for the patients to go to.
    name: Name of the place to make the appointment to.
    starting_time: Time the vaccinating service starts in this place. Could be used to compute available slots.
    ending_time: Time the vaccinating service ends in this place. Could be used to compute available slots.
    capacity: Simultaneous slots that a place has as any given time. Could be used to compute available slots.
    """

    name = models.CharField(null=False, blank=False, max_length=50)

    # Todo: TBD
    starting_time = models.TimeField(null=False, blank=False)
    ending_time = models.TimeField(null=False, blank=False)
    capacity = models.IntegerField(null=False, blank=False, default=1)

    def __str__(self):
        return f"{self.name}"


class Patient(Model):
    """This model should hold the info about every patient that has ever requested to ask for a service.
    *nhc* should be the national healthcare unique identifier for each person.
    """

    name = models.CharField(null=False, blank=False, max_length=50)
    surname1 = models.CharField(null=False, blank=False, max_length=50)
    surname2 = models.CharField(null=True, blank=True, max_length=50)
    nhc_code = models.CharField(null=False, blank=False, max_length=100)
    # TODO: some kind of email double-check should be implanted. May it given requiring accessing a confirmation link,
    #  or might only be simply inputting the email twice. Preferibly both. Great part of this could be absorved into
    #  'user' model once a Login system is implemented.
    email = models.EmailField()

    class Meta:
        verbose_name_plural = "Patients"

    def __str__(self):
        return (f"{self.name} "
                f"{str(self.surname1)} "
                f"{str(self.surname2)+' ' if self.surname2 else ''}"
                f"({self.pk})")


class Reservation(Model):
    """
    Reservations core table. Each entry is composed by:
    patient: pk relation to the Patient table, that contains the relevant patient info.
    place: pk relation to the Place table, specifying where the appointment is set to be attended.
    date: datetime field specifying when the appointment will be done.
    code: a random string unique for each appointment. Could be handled to a patient via a QR code in a mail.
       This is the code each patient should present with themselves when they attend their appointment.
    confirmed: a boolean field that will say whether the patient has confirmed his appointment. TBD
    attended: a boolean field that will only be set to true the moment the patient validates the date.

    created: a datetime field that says when this entry was added to the database
    modified: a datetime field that says when this entry was last modified
    """
    patient = models.ForeignKey(Patient, related_name='reservations', null=False, blank=False,
                                on_delete=models.CASCADE, max_length=50)
    place = models.ForeignKey(VaccinationLocation, related_name='reservations', null=False, blank=False,
                              on_delete=models.CASCADE, max_length=50)
    date = models.DateTimeField(null=False, blank=False)

    validation_code = models.CharField(null=True, blank=True, max_length=100, unique=True)

    # TODO: confirmed field could be used to send a confirmation request to the patient few days before the appointment
    #  (To Be Done)
    confirmed = models.BooleanField(default=False)
    attended = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = "Reservations"
