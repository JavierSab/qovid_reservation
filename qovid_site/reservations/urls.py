"""
URLS for the Reservations App
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from .views import VaccinationPlaceList
from .views import create_reservation, validate_reservation
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    # Functionalities
    path('reservation/create/', create_reservation, name="Reservations Creation"),
    path('validations/validate/', validate_reservation, name="Patient Validation"),

    # Models CRUD
    path('vaccinationPlaces/list/', csrf_exempt(VaccinationPlaceList.as_view()), name="Vaccination"),

]
