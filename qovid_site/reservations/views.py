from django.shortcuts import render

from .models import *
from django.http import HttpResponse, JsonResponse
from rest_auth.views import LoginView
from rest_framework import viewsets, generics
from django.views.decorators.http import require_http_methods
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from .serializers import VaccinationLocationSerializer

from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
import datetime
from django.utils import timezone

import hashlib
import random
import string


# Create your views here.

def externally_verificate_patient(nhc):
    """
    Requests for an evaluation against a
    :param nhc:
    :return: Bool with True if the patient is valid, False otherwhise
    """
    # TODO: implement proper integration
    # TODO: probably a nhc is not a thing that an external service as this one should store in a database. A hash or any
    #  unique id for each patient should be more desireable, or even nothing at all. This function could be a good place
    #  for asking for such hash as that.
    return True


def check_patient(name, surname1, surname2, nhc):
    """
    Takes the full name of the patient and their nhc. If the nhc is valid, creates and returns an entry in our db.
    :param name:
    :param surname1:
    :param surname2:
    :param nhc:
    :return:
    """

    try:
        if externally_verificate_patient(nhc):
            patient, created = Patient.objects.get_or_create(nhc_code=nhc)
            patient.name = name
            patient.surname1 = surname1
            patient.surname2 = surname2

            patient.save()

            patient = Patient.objects.get(nhc_code=nhc)
            success = True
        else:
            patient = None
            success = False
    except Exception as e:
        patient = None
        success = False

    return {'success': success, 'patient': patient}


def check_place(proposed_place):
    """

    :param proposed_place:
    :return:
    """

    try:
        proposed_place_pk = int(proposed_place)
        place = VaccinationLocation.objects.get(pk=proposed_place_pk)
        success = True
    except (ObjectDoesNotExist, MultipleObjectsReturned) as e:
        place = None
        success = False

    return {'success': success, 'place': place}


def check_date(date):
    """
    Validates and parses the date provided by frontend to a proper DT field object.

    :param date:
    :return:
    """
    # TODO: assume everything is UTC. If frontent in local tz, assume it parses the date before sending it to backend.

    try:
        parsed_date = datetime.datetime.strptime(date, '%Y/%m/%d %H:%M:%S')
        success = True
    except Exception as e:
        parsed_date = None
        success = False

    return {'success': success, 'date': parsed_date}


def create_code_for_patient(patient, date):
    """
    Creates a long code that uniquely identifies an appointment.

    For the moment is a hash of the patient and the reservation date plus a random string, but it could likewise be
    any random generated string.

    :return: string code
    """

    patient_str = f"{patient.name}{patient.surname1}{patient.surname2 if patient.surname2 else ''}"
    date_str = f"{date}"
    random_str = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))
    hash_object = hashlib.sha256(f"{patient_str}{date_str}{random_str}".encode())
    hex_dig = hash_object.hexdigest()

    return hex_dig[:20].upper()  # 15**20 ~= 3e23 >> 6e18 (2b people 1 second 100 years test)


@csrf_exempt
@require_http_methods(['POST'])
def create_reservation(request):
    """
    This function should create the appointment, once the patient has fulfilled the necessary information.

    The code should be handed to the patient, either via email or in a successive call from the frontend. This code
    could be handed as a plain string or as a QR picture.

    Patient data: As long as a login system is not installed, we must assume every patient must provide their data every
    time they try to make an appointment.
    Reservation date: The date should be fulfilled in frontend, but we can check for proper formatting here.
    Reservation place: We should trust they've selected the reservation place from the options provided to them in the
    frontend, so we should be able to expect the pk of the place.
    :param request:
    :return:
    """

    # Recieve information
    patient_name = request.POST.get('patient_name')
    patient_surname1 = request.POST.get('patient_surname1')
    patient_surname2 = request.POST.get('patient_surname2')
    patient_nhc_code = request.POST.get('patient_nhc_code')

    reservation_place = request.POST.get('reservation_place')
    reservation_date = request.POST.get('reservation_date')

    patient_checked = check_patient(patient_name, patient_surname1, patient_surname2, patient_nhc_code)
    if not patient_checked['success']:
        message = "Patient data invalid. Client data failed or NHC service invalid."
        return JsonResponse({'message': message, 'validation_code': None, 'success': False},
                            status=401, )

    place_checked = check_place(reservation_place)
    if not place_checked['success']:
        message = "Submitted place is invalid. Please check the information provided."
        return JsonResponse({'message': message, 'validation_code': None, 'success': False},
                            status=401, )

    date_checked = check_date(reservation_date)
    if not date_checked['success']:
        message = "Submitted date is invalid. Please check the information provided."
        return JsonResponse({'message': message, 'validation_code': None, 'success': False},
                            status=401, )

    validation_code = create_code_for_patient(patient_checked['patient'], date_checked['date'])

    reservation = Reservation.objects.create(patient=patient_checked['patient'],
                                             place=place_checked['place'],
                                             date=date_checked['date'],
                                             validation_code=validation_code,

                                             attended=False, confirmed=False)

    message = (f"Appointment successfully created on {date_checked['date']}(UTC) at {place_checked['place'].name}."
               f"\nYour validation code is {validation_code}")
    return JsonResponse({'message': message, 'validation_code': validation_code, 'success': False},
                        status=200)

# ValidatePatient view
@csrf_exempt
@require_http_methods(['POST'])
def validate_reservation(request):
    """
    This view is responsible for validating the patients that arrive at the vaccination places with a reservation.

    For this, a connected device (pc, raspberry, android...) must be in place with some kind of input method
    (a QR scanner connected to a device could do the trick). For covid, it is preferible touchless interaction.
    If it is not possible, then interaction with flat surfaces (screens, capacitive buttons) is prefereible over
    keyboards as they are more easily cleaned.


    :param request:
    :return:
    """

    # TODO: check we are in a range of time near the booked hour.

    patient_nhc_code = request.POST.get('patient_nhc_code')
    validation_code = request.POST.get('validation_code')

    try:
        reservation = Reservation.objects.get(attended=False,
                                              patient__nhc_code=patient_nhc_code,
                                              validation_code=validation_code)

        reservation.attended = True
        reservation.save()

        message = "Your reservation has been successfully validated"
        return JsonResponse({'message': message, 'success': True}, status=200)

    except ObjectDoesNotExist:
        message = "No reservation found for provided credentials"
        return JsonResponse({'message': message, 'success': False}, status=401)


# LISTING CLASSES

# VaccinationLocation list
@method_decorator(csrf_exempt, name='dispatch')
class VaccinationPlaceList(generics.ListAPIView):
    queryset = VaccinationLocation.objects.all()
    serializer_class = VaccinationLocationSerializer
