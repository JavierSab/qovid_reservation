from rest_framework import serializers
from .models import *


class VaccinationLocationSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='pk')

    class Meta:
        model = VaccinationLocation
        fields = ['id', 'name', 'starting_time', 'ending_time', 'capacity']